# -*- coding: utf-8 -*-
{
    'name': "intranet_invoices",

    'summary': """
        Module REST to display invoices""",

    'description': """
    """,

    'author': "Som Mobilitat",
    'website': "http://www.sommobilitat.coop",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account'],

    # always loaded
    'data': [
    ],
}
